import tensorflow as tf
from pixelnet import pixelnet


def main():
    num_classes = 21
    images = tf.placeholder(tf.float32, shape=[4, 224, 224, 3], name='images')
    labels = tf.placeholder(tf.int32, shape=[4, 224, 224, 1], name='labels')
    index = tf.placeholder(tf.int32, shape=[4000, 3], name='index')
    h, y = pixelnet(images=images, labels=labels, index=index, num_classes=num_classes)
    y = tf.one_hot(y, num_classes)
    loss = tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=h)

    sess = tf.InteractiveSession()
    summary = tf.summary.FileWriter('summary', graph=sess.graph)
    summary.add_graph(sess.graph)


if __name__ == '__main__':
    main()
